package com.revolut.test.sdk.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.test.sdk.models.ExchangeRatesModel;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Backend Api
 *
 * Used as mediation layer between the APP and SERVER
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
class BackendApi {

    private final static String BASE_URL = "https://revolut.duckdns.org/";

    private ExchangeRatesClient exchangeRatesClient;

    BackendApi() {
        initBackendApi();
    }

    private void initBackendApi() {
        final Retrofit.Builder builder = constructBuilder();
        exchangeRatesClient = builder.baseUrl(BASE_URL).build().create(ExchangeRatesClient.class);
    }

    Observable<ExchangeRatesModel> getLatestExchangeRates(final String base) {
        return Observable.interval(0, 1000, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap(interval -> exchangeRatesClient.getLatestExchangeRates(base));
    }

    private Retrofit.Builder constructBuilder() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.HEADERS);
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        final Gson gson = new GsonBuilder().create();
        final GsonConverterFactory gsonFactory = GsonConverterFactory.create(gson);

        return new Retrofit.Builder()
                .addConverterFactory(gsonFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build());
    }
}
