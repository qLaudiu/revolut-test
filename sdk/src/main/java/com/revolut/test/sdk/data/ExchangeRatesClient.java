package com.revolut.test.sdk.data;

import com.revolut.test.sdk.models.ExchangeRatesModel;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Exchange Rates Retrofit interface
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
interface ExchangeRatesClient {

    @GET("latest")
    Observable<ExchangeRatesModel> getLatestExchangeRates(@Query("base") final String base);
}
