package com.revolut.test.sdk.utils;

/**
 * String utility class
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public final class StringUtils {

    private StringUtils() {
        // Private Constructor
    }

    public static boolean isNullOrEmpty(final String s) {
        return s == null || s.isEmpty();
    }
}
