package com.revolut.test.sdk.models;

import java.util.HashMap;

/**
 * Exchange Rates Model
 * Model used to map the json into a object
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public class ExchangeRatesModel {

    private String base;

    private String date;

    private HashMap<String, Double> rates;

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public HashMap<String, Double> getRates() {
        return rates;
    }
}
