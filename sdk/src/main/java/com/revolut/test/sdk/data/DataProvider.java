package com.revolut.test.sdk.data;

import com.revolut.test.sdk.models.ExchangeRatesModel;

import io.reactivex.Observable;

/**
 * DataProvider
 *
 * Used as facade for UI.
 * From this class the UI can fetch different types of data
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public final class DataProvider {

    private static DataProvider dataProviderInstance;

    private final BackendApi backendApi = new BackendApi();

    private DataProvider() {
        // Private constructor
    }

    public static DataProvider getInstance() {
        if (dataProviderInstance == null) {
            dataProviderInstance = new DataProvider();
        }
        return dataProviderInstance;
    }

    public Observable<ExchangeRatesModel> getLatestExchangeRates(final String base) {
        return backendApi.getLatestExchangeRates(base);
    }
}
