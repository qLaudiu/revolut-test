package com.revolut.test;

import android.app.Activity;
import android.os.Bundle;

import com.revolut.test.ui.UINavigator;

/**
 * Main Activity
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public class MainActivity extends Activity {

    private final UINavigator uiNavigator = new UINavigator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uiNavigator.showExchangeRatesFragment(this, getFragmentManager(), "EUR");
    }
}
