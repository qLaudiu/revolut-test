package com.revolut.test.ui;

/**
 * Base class used for view models
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public abstract class BaseViewModel<T> {

    protected final String TAG = this.getClass().getCanonicalName();

    protected abstract T getData();
}
