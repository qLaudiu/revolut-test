package com.revolut.test.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;

import com.revolut.test.R;
import com.revolut.test.sdk.utils.StringUtils;
import com.revolut.test.ui.exchange.ExchangeRatesFragment;

import static com.revolut.test.ui.BaseFragment.EXTRA_STRING_BASE_RATE;

/**
 * Utility class used for navigation
 *
 * @author Claudiu Bugeac on 2019-07-15.
 */
public final class UINavigator {

    public void showExchangeRatesFragment(final Context context, final FragmentManager fragmentManager, final String baseRate) {
        Fragment fragment;
        if (StringUtils.isNullOrEmpty(baseRate)) {
            fragment = instantiateFragment(context, ExchangeRatesFragment.class);
        } else {
            final Bundle bundle = new Bundle();
            bundle.putString(EXTRA_STRING_BASE_RATE, baseRate);
            fragment = instantiateFragment(context, ExchangeRatesFragment.class, bundle);
        }
        fragmentManager.beginTransaction()
                .replace(R.id.exchange_frame, fragment)
                .commit();
    }

    private Fragment instantiateFragment(final Context context, final Class fragmentClass) {
        return instantiateFragment(context, fragmentClass, null);
    }

    private Fragment instantiateFragment(final Context context, final Class fragmentClass, final Bundle bundle) {
        return Fragment.instantiate(context, fragmentClass.getCanonicalName(), bundle);
    }
}
