package com.revolut.test.ui.exchange;

import java.util.List;

/**
 * Interface Exchange Rates Listener
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public interface IExchangeRatesListener {

    void onBaseRateChanged(double value, List<Rate> rates);

    void onBaseValueChanged(double value);
}
