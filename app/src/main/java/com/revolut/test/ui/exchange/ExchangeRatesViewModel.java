package com.revolut.test.ui.exchange;

import android.util.Log;

import com.revolut.test.R;
import com.revolut.test.sdk.data.DataProvider;
import com.revolut.test.sdk.models.ExchangeRatesModel;
import com.revolut.test.ui.BaseViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observable;

/**
 * View Model used to get the data from BE and map the data in order to be used on UI
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public class ExchangeRatesViewModel extends BaseViewModel<Observable<List<Rate>>> {

    private final String baseRate;

    private final double baseValue;

    private final List<Rate> keys;

    ExchangeRatesViewModel(final String baseRate, final double baseValue, final List<Rate> keys) {
        this.baseRate = baseRate;
        this.baseValue = baseValue;
        this.keys = keys;
    }

    @Override
    protected Observable<List<Rate>> getData() {
        return DataProvider.getInstance()
                .getLatestExchangeRates(baseRate)
                .map(this::transformLatestRates)
                .map(this::sortRates);
    }

    private List<Rate> transformLatestRates(ExchangeRatesModel exchangeRatesModel) {
        if (exchangeRatesModel == null) {
            Log.w(TAG, "ExchangeRatesModel is null, can't transform to list of rate");
        } else {
            final List<Rate> rates = new ArrayList<>(exchangeRatesModel.getRates().size() + 1);
            final Rate rate = new Rate(getResourceImage(exchangeRatesModel.getBase()),
                    exchangeRatesModel.getBase(), "Full Name", baseValue);
            rates.add(rate);
            for (Map.Entry item : exchangeRatesModel.getRates().entrySet()) {
                rates.add(new Rate(getResourceImage((String) item.getKey()), (String) item.getKey(),
                        "Full Name", (double) item.getValue() * baseValue));
            }
            return rates;
        }
        return Collections.emptyList();
    }

    private List<Rate> sortRates(List<Rate> rates) {
        if (keys == null || keys.isEmpty()) {
            return rates;
        } else {
            final List<Rate> sortedRates = new ArrayList<>(rates.size());
            for (Rate rKey : keys) {
                for (Rate r : rates) {
                    if (Objects.equals(rKey.getShortName(), r.getShortName())) {
                        sortedRates.add(r);
                        break;
                    }
                }
            }
            return sortedRates;
        }
    }

    private int getResourceImage(final String rate) {
        switch (rate) {
            case "EUR":
                return R.mipmap.eur;
            case "RON":
                return R.mipmap.ron;
            case "JPY":
                return R.mipmap.jpn;
            case "GBP":
                return R.mipmap.uk;
            case "USD":
                return R.mipmap.usd;
            case "AUD":
                return R.mipmap.aud;
            case "CHF":
                return R.mipmap.chf;
            case "CNY":
                return R.mipmap.cny;
            case "ILS":
                return R.mipmap.ils;
            default:
                return -1;
        }
    }
}
