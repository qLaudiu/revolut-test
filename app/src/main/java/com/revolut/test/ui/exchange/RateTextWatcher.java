package com.revolut.test.ui.exchange;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.revolut.test.sdk.utils.StringUtils;

/**
 * Rate Text Watcher
 *
 * @author Claudiu Bugeac on 2019-07-15.
 */
public class RateTextWatcher implements TextWatcher {

    private final static String TAG = RateTextWatcher.class.getCanonicalName();

    private IExchangeRatesListener iExchangeRatesListener;

    void setIExchangeRatesListener(IExchangeRatesListener iExchangeRatesListener) {
        this.iExchangeRatesListener = iExchangeRatesListener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // No need for this
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // No need for this
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (iExchangeRatesListener == null) {
            Log.w(TAG, "IExchangeRatesListener is null");
        } else {
            Log.d(TAG, "After text changed: " + s.toString());
            final String value = s.toString();
            if (StringUtils.isNullOrEmpty(value)) {
                iExchangeRatesListener.onBaseValueChanged(0);
            } else {
                iExchangeRatesListener.onBaseValueChanged(Double.valueOf(value));
            }
        }
    }
}
