package com.revolut.test.ui.exchange;

import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.revolut.test.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Exchange Rate Adapter
 * Used to show the exchange rates list
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public class ExchangeRatesAdapter extends RecyclerView.Adapter<ExchangeRatesAdapter.ViewHolder> {

    private final static String TAG = ExchangeRatesAdapter.class.getCanonicalName();

    private List<Rate> rateList = new ArrayList<>();

    private IExchangeRatesListener iExchangeRatesListener;

    private ViewHolder baseRateHolder;

    private final RateTextWatcher rateTextWatcher = new RateTextWatcher();

    void setIExchangeRatesListener(IExchangeRatesListener iExchangeRatesListener) {
        this.iExchangeRatesListener = iExchangeRatesListener;
        this.rateTextWatcher.setIExchangeRatesListener(this.iExchangeRatesListener);
    }

    void setRateList(List<Rate> rateList) {
        this.rateList = rateList;
        notifyItemRangeChanged(1, this.rateList.size() - 1);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rate_item, viewGroup, false));
    }

    @Override
    public int getItemCount() {
        return rateList.isEmpty() ? 0 : rateList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final Rate rate = rateList.get(position);
        if (rate == null) {
            Log.w(TAG, "Rate from position: " + position + " is null, can't bind the view");
        } else {
            bindRate(viewHolder, rate, position);
        }
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
        holder.rateExchangeInput.removeTextChangedListener(rateTextWatcher);
        holder.rateImage.setImageResource(0);
    }

    private void bindRate(@NonNull ViewHolder viewHolder, @NonNull Rate rate, int position) {
        bindImage(viewHolder, rate.getImageRes());
        bindName(viewHolder, rate.getShortName(), rate.getFullName());
        bindExchangeInput(viewHolder, rate.getValue());
        setListeners(viewHolder, position);
    }

    private void bindImage(@NonNull ViewHolder viewHolder, @DrawableRes int rateImageRes) {
        if (rateImageRes != -1) {
            viewHolder.rateImage.setImageResource(rateImageRes);
        }
    }

    private void bindName(@NonNull ViewHolder viewHolder, String shortName, String name) {
        viewHolder.rateShortName.setText(shortName);
        viewHolder.rateName.setText(name);
    }

    private void bindExchangeInput(@NonNull ViewHolder viewHolder, double rageExchange) {
        final NumberFormat decimalFormat = DecimalFormat.getInstance();
        decimalFormat.setMinimumFractionDigits(0);
        viewHolder.rateExchangeInput.setText(decimalFormat.format(rageExchange));
    }

    private void setListeners(@NonNull ViewHolder viewHolder, final int position) {
        viewHolder.itemView.setOnClickListener(v -> {
            if (iExchangeRatesListener == null) {
                Log.w(TAG, "IExchangeRatesListener is null");
            } else {
                final String rateValue = viewHolder.rateExchangeInput.getText().toString();
                Log.d(TAG, "Item Clicked - position: " + position + " value: " + rateValue);
                removeTextListenerFromBase();
                onItemMove(position);
                iExchangeRatesListener.onBaseRateChanged(Double.valueOf(rateValue), rateList);
            }
        });
        if (position == 0) {
            baseRateHolder = viewHolder;
            viewHolder.rateExchangeInput.addTextChangedListener(rateTextWatcher);
        }
    }

    private void onItemMove(int fromPosition) {
        for (int i = fromPosition; i > 0; i --) {
            Collections.swap(rateList, i, i - 1);
        }
        notifyItemMoved(fromPosition, 0);
        notifyItemChanged(fromPosition);
        notifyItemChanged(0);
    }

    private void removeTextListenerFromBase() {
        if (baseRateHolder == null) {
            Log.d(TAG, "The base rate holder is null");
        } else {
            baseRateHolder.rateExchangeInput.removeTextChangedListener(rateTextWatcher);
            baseRateHolder = null;
        }
    }

    final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rate_image)
        ImageView rateImage;

        @BindView(R.id.rate_short_name)
        TextView rateShortName;

        @BindView(R.id.rate_name)
        TextView rateName;

        @BindView(R.id.rate_exchange_input)
        EditText rateExchangeInput;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
