package com.revolut.test.ui.exchange;

import androidx.annotation.DrawableRes;

/**
 * UI Model used to show the Rates
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
class Rate {

    private @DrawableRes int imageRes;

    private String shortName;

    private String fullName;

    private double value;

    @DrawableRes int getImageRes() {
        return imageRes;
    }

    String getShortName() {
        return shortName;
    }

    String getFullName() {
        return fullName;
    }

    double getValue() {
        return value;
    }

    Rate(@DrawableRes int imageRes, String shortName, String fullName, double value) {
        this.imageRes = imageRes;
        this.shortName = shortName;
        this.fullName = fullName;
        this.value = value;
    }
}
