package com.revolut.test.ui.exchange;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.revolut.test.R;
import com.revolut.test.ui.BaseFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Exchange Rates Fragment
 *
 * @author Claudiu Bugeac on 2019-07-14.
 */
public class ExchangeRatesFragment extends BaseFragment implements IExchangeRatesListener {

    @BindView(R.id.exchange_list)
    RecyclerView exchangeRatesList;

    private double currentBaseRateValue = 1;

    private String currentBaseRate;

    private Disposable ratesDisposable;

    private final ExchangeRatesAdapter exchangeRatesAdapter = new ExchangeRatesAdapter();

    private final String defaultBaseRate = "RON";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle bundle = getArguments();
        if (bundle == null) {
            Log.d(TAG, "The bundle is null, use the default rate as base");
            currentBaseRate = defaultBaseRate;
        } else {
            currentBaseRate = bundle.getString(EXTRA_STRING_BASE_RATE, defaultBaseRate);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_exchange_rates, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        exchangeRatesList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        exchangeRatesList.setAdapter(exchangeRatesAdapter);
        exchangeRatesList.setItemAnimator(null);
        updateRateList(currentBaseRate, currentBaseRateValue, null);
        exchangeRatesAdapter.setIExchangeRatesListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposeRates();
    }

    @Override
    public void onBaseRateChanged(double value, List<Rate> rates) {
        Log.d(TAG, "OnBaseRateChanged - value: " + value);
        exchangeRatesList.scrollToPosition(0);
        currentBaseRate = rates.get(0).getShortName();
        currentBaseRateValue = value;
        updateRateList(currentBaseRate, currentBaseRateValue, rates);
    }

    @Override
    public void onBaseValueChanged(double value) {
        Log.d(TAG, "OnBaseValueChanged: " + value);
        currentBaseRateValue = value;
        updateRateList(currentBaseRate, currentBaseRateValue, null);
    }

    private void updateRateList(final String base, final double baseValue, final List<Rate> keys) {
        disposeRates();
        ratesDisposable = new ExchangeRatesViewModel(base, baseValue, keys)
                .getData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(exchangeRatesAdapter::setRateList,
                        throwable -> Log.e(TAG, "Get rates list error: ", throwable));
    }

    private void disposeRates() {
        if (ratesDisposable != null && !ratesDisposable.isDisposed()) {
            ratesDisposable.dispose();
        }
    }
}
